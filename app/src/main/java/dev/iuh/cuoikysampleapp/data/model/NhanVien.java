package dev.iuh.cuoikysampleapp.data.model;

import androidx.annotation.NonNull;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

@Entity(tableName = "NHANVIEN")
public class NhanVien {
	@ColumnInfo
	@PrimaryKey
	@NonNull
	private String maNV;

	@ColumnInfo
	private String tenNV;

	@ColumnInfo
	private boolean isNam;

	@ColumnInfo
	private String phongBan;

	public String getMaNV() {
		return maNV;
	}

	public void setMaNV(String maNV) {
		this.maNV = maNV;
	}

	public String getTenNV() {
		return tenNV;
	}

	public void setTenNV(String tenNV) {
		this.tenNV = tenNV;
	}

	public boolean isNam() {
		return isNam;
	}

	public void setNam(boolean nam) {
		this.isNam = nam;
	}

	public String getPhongBan() {
		return phongBan;
	}

	public void setPhongBan(String phongBan) {
		this.phongBan = phongBan;
	}

	public NhanVien(String maNV, String tenNV, boolean isNam, String phongBan) {
		this.maNV = maNV;
		this.tenNV = tenNV;
		this.isNam = isNam;
		this.phongBan = phongBan;
	}

	@Override
	public String toString() {
		return maNV + tenNV + (isNam == true ? "Nam" : "Nữ") + phongBan;
	}
}
