package dev.iuh.cuoikysampleapp.adapter;

import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.AsyncListDiffer;
import androidx.recyclerview.widget.DiffUtil;
import androidx.recyclerview.widget.RecyclerView;


import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.List;

import dev.iuh.cuoikysampleapp.data.model.NhanVien;

public class NhanVienAdapter extends RecyclerView.Adapter<NhanVienAdapter.NhanVienViewHolder> {
    private List<NhanVien> mItems;
    private AsyncListDiffer<NhanVien> mDiffer;
    private DiffUtil.ItemCallback<NhanVien> mCallback = new DiffUtil.ItemCallback<NhanVien>() {
        @Override
        public boolean areItemsTheSame(@NonNull @NotNull NhanVien oldItem,
                                       @NonNull @org.jetbrains.annotations.NotNull NhanVien newItem) {
            return oldItem.equals(newItem);
        }

        @Override
        public boolean areContentsTheSame(@NonNull @org.jetbrains.annotations.NotNull NhanVien oldItem, @NonNull @org.jetbrains.annotations.NotNull NhanVien newItem) {
            return oldItem.getMaNV().equals(newItem.getMaNV());
        }
    };
    private OnBaseItemListener onBaseItemListener;

    public NhanVienAdapter(List<NhanVien> mItems,
                           OnBaseItemListener onBaseItemListener) {
        this.mItems = mItems;
        this.onBaseItemListener = onBaseItemListener;
        this.mDiffer = new AsyncListDiffer<>(this, mCallback);
        update(mItems);
    }

    @NonNull
    @NotNull
    @Override
    public NhanVienViewHolder onCreateViewHolder(@NonNull @NotNull ViewGroup parent, int viewType) {
        return null;
    }

    @Override
    public void onBindViewHolder(@NonNull @NotNull NhanVienAdapter.NhanVienViewHolder holder, int position) {

    }

    @Override
    public int getItemCount() {
        return mDiffer.getCurrentList().size();
    }

    public NhanVien getItemAt(int position) {
        return mDiffer.getCurrentList().get(position);
    }

    public List<NhanVien> getItems() {
        return mDiffer.getCurrentList();
    }

    public boolean remove(int position) {
        List<NhanVien> list = new ArrayList<>(mDiffer.getCurrentList());
        if (!list.isEmpty()) {
            list.remove(position);
            update(list);
            return true;
        }
        return false;
    }

    public boolean remove(NhanVien item) {
        List<NhanVien> list = new ArrayList<>(mDiffer.getCurrentList());
        if (!list.isEmpty()) {
            int index = list.indexOf(item);
            if (index >= 0) {
                list.remove(index);
                update(list);
                return true;
            }
            return false;
        }
        return false;
    }

    public void addOrUpdateItem(NhanVien item) {
        List<NhanVien> list = new ArrayList<>(mDiffer.getCurrentList());
        if(!updateItem(item)) {
            list.add(item);
            update(list);
        }
    }

    public boolean updateItem(NhanVien item) {
        List<NhanVien> list = new ArrayList<>(mDiffer.getCurrentList());
        if (!list.isEmpty()) {
            int index = list.indexOf(item);

            if (index >= 0) {
                list.set(index, item);
                update(list);
                return true;
            }
            return false;
        }
        return false;
    }

    public void update(List<NhanVien> newList) {
        mDiffer.submitList(newList);
    }

    public static class NhanVienViewHolder extends RecyclerView.ViewHolder {
        protected OnBaseItemListener onBaseItemListener;


        public NhanVienViewHolder(@NonNull @NotNull View itemView, OnBaseItemListener onBaseItemListener) {
            super(itemView);
            this.onBaseItemListener = onBaseItemListener;
            this.itemView.setOnClickListener(v -> {
                int position = getAdapterPosition();
                if (position != RecyclerView.NO_POSITION) {
                    this.onBaseItemListener.onBaseItemClickedListener(v, position);
                }
            });
        }
    }

    public interface OnBaseItemListener {
        void onBaseItemClickedListener(View view, int position);
        void onBaseItemLongClickedListener(View view, int position);
    }
}
