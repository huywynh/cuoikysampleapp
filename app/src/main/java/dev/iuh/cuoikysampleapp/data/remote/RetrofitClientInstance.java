package dev.iuh.cuoikysampleapp.data.remote;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class RetrofitClientInstance {
    private static final String BASE_URL = "https://jsonplaceholder.typicode.com/";
    private Retrofit retrofit;
    private static RetrofitClientInstance mInstance;

    public RetrofitClientInstance() {
        retrofit = new Retrofit.Builder()
                .baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
    }

    public static synchronized RetrofitClientInstance getInstance() {
        if (mInstance == null) {
            mInstance = new RetrofitClientInstance();
        }
        return mInstance;
    }

    public Retrofit getRetrofit() {
        return retrofit;
    }
}
