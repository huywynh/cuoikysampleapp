package dev.iuh.cuoikysampleapp.data.local;

import android.content.Context;

import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;

import dev.iuh.cuoikysampleapp.data.model.NhanVien;

@Database(entities = {NhanVien.class}, version = 1)
public abstract class MyDatabase extends RoomDatabase {
	public abstract NhanVienDao nhanVienDao();

	public static final String DATABASE_NAME = "QLNV";
	private static volatile MyDatabase INSTANCE;

	public static MyDatabase getDatabase(final Context context) {
		if (INSTANCE == null) {
			synchronized (MyDatabase.class) {
				if (INSTANCE == null) {
					INSTANCE = Room.databaseBuilder(context, MyDatabase.class, DATABASE_NAME)
							//.allowMainThreadQueries()
							.fallbackToDestructiveMigration()
							.build();
				}
			}
		}
		return INSTANCE;
	}
}