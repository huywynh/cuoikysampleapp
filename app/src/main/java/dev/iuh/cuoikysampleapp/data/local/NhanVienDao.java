package dev.iuh.cuoikysampleapp.data.local;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;
import androidx.room.Update;

import java.util.List;

import dev.iuh.cuoikysampleapp.data.model.NhanVien;


@Dao
public interface NhanVienDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    Long themNhanVien(NhanVien nv);

    @Query("SELECT * FROM NHANVIEN WHERE maNV = :ma")
    NhanVien findNhanVienByMa(String ma);

    @Query("SELECT * FROM NHANVIEN WHERE maNV = :keyword OR tenNV LIKE :keyword")
    List<NhanVien> findNhanVienByMaOrTen(String keyword);

    @Query("SELECT * FROM NHANVIEN")
    List<NhanVien> getAllNhanVien();

    @Update
    void updateNhanVien(NhanVien nhanVien);

    @Delete
    void deleteNhanVien(NhanVien nhanVien);
}
